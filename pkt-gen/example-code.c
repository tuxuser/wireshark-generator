/*
 * Copyright 2018, Richard Sharpe
 */

#include "pkt-gen.h"

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Some example code for a simple struct
 */

static bool big_endian = true;
static int pkt_count = 0;

enum funcs_enum {
	CONNECT        = 20,
	CONNECT_ACK    = 21,
	REQUEST_DATA   = 40,
	REQUEST_REPLY  = 41,
	DISCONNECT     = 60,
	DISCONNECT_ACK = 61
};

enum data_enum {
	READ_SHORT  = 0,
	READ_LONG   = 1,
	READ_STRING = 2
};

int gen_add_elt(struct struct_elt *struct_elt, void *elt, enum elt_type type)
{
	int len = 0;

	if (struct_elt->next_elt >= struct_elt->count)
		return -EINVAL;

	struct_elt->elements[struct_elt->next_elt++] = elt;

	switch (type) {
	case ELT_IMMEDIATE:
		struct_elt->len += ((struct element *)elt)->len;
		break;
	case ELT_INDIRECT:
		struct_elt->len += ((struct struct_elt *)elt)->len;
		break;
	case ELT_INDIRECT_DATA:
		struct_elt->len += ((struct indirect_elt *)elt)->len;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

/*
 * Generate a uint23 value according to the params struct and the vals.
 *
 * We keep generating these values in the order provided.
 */

uint32_t pkt_gen_uint32_val(struct params *params, struct value_elts_32 *vals)
{
	/*
	 * Do random at some stage
	 */

	/*
	 * Work through the values we have
	 */
	do {
		uint32_t ret_val;

		/*
		 * Reset it if still being asked
		 */
		if (vals->state >= vals->elt_count)
			vals->state = 0;

		if (!vals->elts[vals->state].valid) {
			/* Skip to the end */
			vals->value = vals->elts[vals->state].max  + 1;
			vals->state++;
			continue;
		}

		/*
		 * We must be on a valid entry now
		 */
		ret_val = vals->value;

		if (vals->elts[vals->state].max  == ret_val) {
			vals->state++;
			vals->value++;
		} else {
			vals->value++;
		}

		return ret_val;

	} while (true);
}

/*
 * Return a random 32-bit value ... later we may apply some restrictions
 */
uint32_t pkt_gen_uint32_random(struct params *params)
{
	return rand();
}

uint16_t pkt_gen_uint16_val(struct params *params, struct value_elts_32 *vals)
{
	uint16_t ret_val;

	ret_val = (uint16_t)pkt_gen_uint32_val(params, vals);

	return ret_val;
}

uint8_t pkt_gen_uint8_val(struct params *params, struct value_elts_32 *vals)
{
	uint8_t ret_val;

	ret_val = (uint8_t)pkt_gen_uint32_val(params, vals);

	return ret_val;
}

struct element *pkt_gen_alloc_elt_uint32_t(uint32_t val, bool big_endian)
{
	struct element *elt = malloc(sizeof(struct  element));

	if (elt == NULL)
		return elt;

	elt->type = ELT_IMMEDIATE;
	elt->len = 4;
	if (big_endian) {
		elt->d.data[3] = val & 0xFF;
		elt->d.data[2] = val >> 8;
		elt->d.data[1] = val >> 16;
		elt->d.data[0] = val >> 24;
	} else {
		elt->d.data[0] = val & 0xFF;
		elt->d.data[1] = val >> 8;
		elt->d.data[2] = val >> 16;
		elt->d.data[3] = val >> 24;
	}

	return elt;
}

struct element *pkt_gen_alloc_elt_uint16_t(uint16_t val, bool big_endian)
{
	struct element *elt = malloc(sizeof(struct  element));

	if (elt == NULL)
		return elt;

	elt->type = ELT_IMMEDIATE;
	elt->len = 2;
	if (big_endian) {
		elt->d.data[1] = val & 0xFF;
		elt->d.data[0] = val >> 8;
	} else {
		elt->d.data[0] = val & 0xFF;
		elt->d.data[1] = val >> 8;
	}

	return elt;
}

struct element *pkt_gen_alloc_elt_uint8_t(uint8_t val)
{
	struct element *elt = malloc(sizeof(struct  element));

	if (elt == NULL)
		return elt;

	elt->type = ELT_IMMEDIATE;
	elt->len = 1;
	elt->d.data[0] = val;
}

struct indirect_elt *gen_invalid_data(struct params *params, uint32_t len)
{
	struct indirect_elt *elt = malloc(sizeof(struct indirect_elt) +
				   len - 1);
	int i;

	if (elt == NULL)
		return elt;

	elt->type = ELT_INDIRECT_DATA;
	elt->len = len;

	for (i = 0; i < len; i++) {
		elt->data[i] = rand() & 0xFF;
	}

	return elt;
}

/*
 * Generate an invalid length ... just make it too large at the moment
 */
uint32_t gen_invalid_len(struct params *params, uint8_t func)
{
	return 20 + func;
}

struct struct_elt *pkt_gen_alloc_struct_elt(uint32_t len)
{
	struct struct_elt *new_elt = NULL;

	new_elt = (struct struct_elt *)malloc(sizeof(struct struct_elt) +
						(sizeof(void *)) * (len - 1));

	new_elt->type = ELT_INDIRECT;  /* A struct */
	new_elt->count = len;
	new_elt->next_elt = 0;

	return new_elt;
}

/* Hmmm, this might not be so good for more general structs */
uint16_t gen_request_reply_len(struct params *params, uint8_t request)
{
	switch (request) {
	case READ_SHORT:
		return 3;
	case READ_LONG:
		return 5;
	case READ_STRING:
		return 16;
	}
}

int gen_request_reply_data(struct params *params, uint8_t request,
	struct struct_elt **result)
{
	struct struct_elt *req_reply_data = pkt_gen_alloc_struct_elt(2);
	struct element *new_elt = NULL;
	struct indirect_elt *data = NULL;
	uint16_t short_datum;
	uint32_t long_datum;
	int ret = 0;

	if (req_reply_data == NULL)
		return -ENOMEM;

	if (params == NULL || result == NULL)
		return -EINVAL;

	new_elt = pkt_gen_alloc_elt_uint8_t(request);
	if (new_elt == NULL)
		return -ENOMEM;
	ret = gen_add_elt(req_reply_data, new_elt, ELT_IMMEDIATE);
	if (ret != 0)
		return ret;

	switch (request) {
	case READ_SHORT:
		short_datum = rand() & 0XFFFF;
		new_elt = pkt_gen_alloc_elt_uint16_t(short_datum, big_endian);
		if (new_elt == NULL)
			return -ENOMEM;
		ret = gen_add_elt(req_reply_data, new_elt, ELT_IMMEDIATE);
		if (ret != 0)
			return ret;
		break;
	case READ_LONG:
		long_datum = rand() & 0XFFFFFFFF;
		new_elt = pkt_gen_alloc_elt_uint32_t(long_datum, big_endian);
		if (new_elt == NULL)
			return -ENOMEM;
		ret = gen_add_elt(req_reply_data, new_elt, ELT_IMMEDIATE);
		if (ret != 0)
			return ret;
		break;

	case READ_STRING:
		data = gen_invalid_data(params, 15);
		if (data == NULL)
			return -ENOMEM;
		ret = gen_add_elt(req_reply_data, data, ELT_INDIRECT_DATA);
		break;
	}

	*result = req_reply_data;

	return ret;
}

struct value_elt_32 func_enum_val[] = {
	{ 0, 19, false },
	{ 20, 20, true },
	{ 21, 21, true },
	{ 22, 39, false },
	{ 40, 40, true },
	{ 41, 41, true },
	{ 42, 59, false },
	{ 60, 60, true },
	{ 61, 61, true },
	{ 62, 255, false },
};
struct value_elts_32 func_enum_vals = {
	ELT_ARRAY_SIZE(func_enum_val),
	func_enum_val,
	0, 0
};
struct value_elt_32  data_enum_val[] = {
	{ 0, 0, true },
	{ 1, 1, true },
	{ 2, 2, true },
	{ 3, 255, false },
};
struct value_elts_32 data_enum_vals = {
	ELT_ARRAY_SIZE(data_enum_val),
	data_enum_val,
	0, 0
};

int gen_grahams_proto_pdu(struct params *params)
{
	int ret = 0;
	uint8_t func;
	uint16_t len;
	uint8_t request;
	uint32_t id = 0;
	struct element *new_elt = NULL;
	struct struct_elt *grahams_proto_pdu = NULL;
	struct struct_elt *request_reply = NULL;
	struct indirect_elt *invalid_pdu = NULL;
	bool do_extra_data = ((rand() % pkt_count) < (pkt_count / 4));

	/* Allocate space? If fixed size, easy. If not, allocate up
	 * Hmmm, have an array of elts, because we know how many,
	 * including structure and unions etc.
	 */

	grahams_proto_pdu = pkt_gen_alloc_struct_elt(do_extra_data ? 4 : 3);

	/* Figure out the command */
	func = pkt_gen_uint8_val(params, &func_enum_vals);
	new_elt = pkt_gen_alloc_elt_uint8_t(func);
	if (new_elt == NULL)
		return -ENOMEM;
	ret = gen_add_elt(grahams_proto_pdu, new_elt, ELT_IMMEDIATE);
	if (ret != 0)
		return ret;
	len = 3;
	switch (func) {
	case REQUEST_DATA:
	case REQUEST_REPLY:
		request = pkt_gen_uint8_val(params, &data_enum_vals);
	default:
		break;
	}

	/*
	 * This has to change to allow the description to specify which
	 * fields are included in the length.
	 */
	switch (func) {
        case CONNECT:
	case CONNECT_ACK:
	case DISCONNECT:
	case DISCONNECT_ACK:
		len += 4;
		break;
	case REQUEST_DATA:
		len += 1;
		break;
	case REQUEST_REPLY:
		len += gen_request_reply_len(params, request);
	default:
		len += gen_invalid_len(params, func);
	}

	new_elt = pkt_gen_alloc_elt_uint16_t(len, big_endian);
	ret = gen_add_elt(grahams_proto_pdu, new_elt, ELT_IMMEDIATE);

	/* Now generate the rest */
	switch (func) {
	case CONNECT:
	case CONNECT_ACK:
	case DISCONNECT:
	case DISCONNECT_ACK:
		id = pkt_gen_uint32_random(params);
		new_elt = pkt_gen_alloc_elt_uint32_t(id, big_endian);
		if (new_elt == NULL)
			return -ENOENT;
		ret = gen_add_elt(grahams_proto_pdu, new_elt, ELT_IMMEDIATE);
		if (ret != 0)
			return ret;
		break;
	case REQUEST_DATA:
		new_elt = pkt_gen_alloc_elt_uint8_t(request);
		if (new_elt == NULL)
			return -ENOMEM;
		ret = gen_add_elt(grahams_proto_pdu, new_elt, ELT_IMMEDIATE);
		if (ret != 0)
			return ret;
		break;
	case REQUEST_REPLY:
		ret = gen_request_reply_data(params, request, &request_reply);
		if (ret != 0)
			return ret;
                ret = gen_add_elt(grahams_proto_pdu, request_reply, ELT_INDIRECT);
		if (ret != 0)
			return ret;
		break;
	default:
		invalid_pdu = gen_invalid_data(params, len);
		if (invalid_pdu == NULL)
			return -ENOMEM;
		ret = gen_add_elt(grahams_proto_pdu, invalid_pdu, ELT_INDIRECT_DATA);
		if (ret != 0)
			return ret;
		break;
	}

	if (do_extra_data) {
		invalid_pdu = gen_invalid_data(params, pkt_count);
		if (invalid_pdu == NULL)
			return -ENOMEM;
		ret = gen_add_elt(grahams_proto_pdu, invalid_pdu, ELT_INDIRECT_DATA);
		if (ret != 0)
			return ret;
	}

	/* Now save the packet. */
	ret = params->pkt_gen_pkt_callback(params, NETWORK_LAYER,
					   grahams_proto_pdu);

	return ret;
}

/*
 * Init the pkt gen routines. Nothing to do here at the moment?
 */
int pkt_gen_init(struct params *params, void **ctx)
{
	*ctx = NULL;

	params->ether_type[0] = 0x89;
	params->ether_type[1] = 0x3c;
        pkt_count = params->packet_count;
	return 0;
}

/*
 * Perform startup. If handling sessions start them here.
 */
int pkt_gen_startup(struct params *params, void *ctx)
{
	return 0;
}

/*
 * Generate the packets ...
 */
int pkt_gen_body(struct params *params, void *ctx)
{
	int ret = 0;
	int count = 0;

	while (params->packet_count--) {
		if ((ret = gen_grahams_proto_pdu(params)) != 0)
			return ret;

		count++;
		printf("Generated packet %d\n", count);
	}

	return 0;
}

/*
 * The shutdown stuff
 */
int pkt_gen_shutdown(struct params *params, void *ctx)
{
	return 0;
}

/*
 * Close off anything.
 */
void pkt_gen_close(struct params *params, void *ctx)
{
	return;
}

/*
 * Get error info
 */
char *pkt_gen_geterr(struct params *params, void *ctx)
{
	return "No error so far";
}
