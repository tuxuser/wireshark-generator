/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

class LuaGenerator extends Generator {

    public LuaGenerator(String inputFile) {
        super(inputFile);
    }

    void generateOnePrelude(ProtoDetails proto) {
        String name = Util.printName(proto.shortName);

        System.out.println(name + "_proto = Proto(" +
                           Util.quoteString(name) + ", " +
                           proto.name + ")"); 
    }

    void generateOneProtoFields(ProtoDetails proto) {
        ArrayList<String> structsLeft = new ArrayList<String>();
        ArrayList<String> fieldNames = new ArrayList<String>();
        ArrayList<String> expertNames = new ArrayList<String>();
        String protoName = Util.printName(proto.shortName);

        structsLeft.add(proto.dissector_entry);

        Debug.println(10, "--ProtoDetails dissector entry: " +
                      proto.dissector_entry);
        while (!structsLeft.isEmpty()) {
            StructInfo struct = structs.get(structsLeft.get(0));
            Debug.println(10, "--Next struct: " + structsLeft.get(0));
            structsLeft.remove(0);

            Debug.println(10, "--Struct " + struct);
            Debug.println(10, "--About to handle struct " + protoName);
            struct.generateLuaStructFields(protoName, protoName, structsLeft,
			    		   fieldNames, expertNames);
            Debug.println(10, "--After one struct, structsLeft.isEmpty(): " +
                              structsLeft.isEmpty());
        }

        System.out.println("");

        // Now put them into the proto list of fields
        String name = Util.printName(proto.shortName);
        System.out.println(name + "_proto.fields = {");
        for (String str:fieldNames) {
            System.out.println("  " + str + ",");
        }
        System.out.println("}\n");

        // Now the expert infos
        System.out.println(name + "_proto.experts = {");
        for (String str:expertNames) {
            System.out.println("  " + str + ",");
        }
        System.out.println("}\n");
    }

    void generateOneProtoStructs(ProtoDetails proto) {
        ArrayList<String> structsLeft = new ArrayList<String>();
        String protoName = Util.printName(proto.shortName);

        structsLeft.add(proto.dissector_entry);

        while (!structsLeft.isEmpty()) {
            StructInfo struct = structs.get(structsLeft.get(0));
            structsLeft.remove(0);

            struct.generateLuaStructDissect(protoName, structsLeft);
        }
    }

    void generateOneTrailer(ProtoDetails proto) {
        String name = Util.printName(proto.shortName);

        System.out.println("ent_table = DissectorTable.get(" +
                           proto.dissector_table + ")");
        System.out.println("ent_table:add(" +
                           Util.unquoteString(proto.dissector_index) +
                           ", " + name + "_proto)");
    }

    void generateEnumDecl(EnumInfo enumInfo) {
        System.out.println("local " + enumInfo.name + " = {");
        for (EnumElt elt:enumInfo.elts) {
            System.out.println("  [" + elt.val + "] = " + elt.string + ",");
        }
        System.out.println("}\n");

        // Now define some local variables
        for (EnumElt elt:enumInfo.elts) {
            System.out.println("local " + elt.id + " = " + elt.val);
        }
        System.out.println("");
    }

    void generateCode() {
        System.out.println("-- LUA Dissector generated from " + inputFile);
        System.out.println("");

	for (String key:protoDetails.keySet()) {
            generateOnePrelude(protoDetails.get(key));
        }

        // Generate stuff for enums
        for (String key:enums.keySet()) {
            generateEnumDecl(enums.get(key));
        }

        // Generate field variables etc.
        for (String key:protoDetails.keySet()) {
            generateOneProtoFields(protoDetails.get(key));
        }

        // Generate structs
        for (String key:protoDetails.keySet()) {
            generateOneProtoStructs(protoDetails.get(key));
        }

        System.out.println("-- Doing registration code");
        // Generate the registration code
        for (String key:protoDetails.keySet()) {
            generateOneTrailer(protoDetails.get(key));
        }
    }
}
