/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

enum EndianType {
    ENDIAN_LITTLE,
    ENDIAN_BIG
}

enum PathType {
    PATH_ABS,                // First three do not have names
    PATH_RELATIVE_PARENT,
    PATH_RELATIVE_CURRENT,
    PATH_COMPONENT           // Must have a name
}

enum CodeGenType {
    C_CODE_GEN,            // Generate a C dissector
    LUA_CODE_GEN,          // Generate a LUA dissector
    PKT_CODE_GEN           // Generate a packet generator
};
