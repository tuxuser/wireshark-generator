/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

class PktGenerator extends Generator {

    public PktGenerator(String inputFile) {
        super(inputFile);
    }

    void generateCode() {
        System.out.println("// Packet Generator generated from " + inputFile);
        System.out.println("// No code generated yet");

        for (String key:structs.keySet()) {
            System.out.println("  // Struct: " +
                              structs.get(key).name);
        }
    }
}
