/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.*;

enum ControlOp {
    OP_NOOP,
    OP_EQ,
    OP_NE,
    OP_GE,
    OP_LE,
    OP_LS,
    OP_RS,
    OP_SUB,
    OP_ADD,
    OP_MUL,
    OP_AND
}

// An expression as used in a switch statement or an array specification
// It would have an op, which can be NOOP, in which case there will
// be nothing to operate on and just the value of the ProtoPath would
// be used. Also, the expression can be empty/null, in which case the
// value is used (only in arrays) as the array bound/length.
class Expression {
    ProtoPath path;
    ControlOp op;
    String value;  // The INT or ID being compared to.

    // Create an expression with just a value
    public Expression(String value) {
        op = ControlOp.OP_NOOP;
        this.value = value;
    }

    // Create an instance from a switchStructEltCtrl
    public Expression(WiresharkGeneratorParser.SwitchStructEltCtrlContext ctx) {
        op = ControlOp.OP_NOOP;

        if (ctx.fieldPath() == null)
            Debug.println(10, "// There does not seem to be a field path");

        path = new ProtoPath(ctx.fieldPath());

        if (ctx.op != null) {
            switch (ctx.op.getType()) {
            case WiresharkGeneratorParser.EQ:
                op = ControlOp.OP_EQ;
                break;
            case WiresharkGeneratorParser.NE:
                op = ControlOp.OP_NE;
                break;
            case WiresharkGeneratorParser.GE:
                op = ControlOp.OP_GE;
                break;
            case WiresharkGeneratorParser.LE:
                op = ControlOp.OP_LE;
                break;
            case WiresharkGeneratorParser.LS:
                op = ControlOp.OP_LS;
                break;
            case WiresharkGeneratorParser.RS:
                op = ControlOp.OP_RS;
                break;
            case WiresharkGeneratorParser.SUB:
                op = ControlOp.OP_SUB;
                break;
            case WiresharkGeneratorParser.ADD:
                op = ControlOp.OP_ADD;
                break;
            case WiresharkGeneratorParser.MUL:
                op = ControlOp.OP_MUL;
                break;
            case WiresharkGeneratorParser.AND:
                op = ControlOp.OP_AND;
                break;
            }

            // There will only be one of these if there is an OP.
            if (ctx.INT() != null) {
                value = ctx.INT().getText();
            } else {
                value = ctx.ID().getText();
            }
        }
    }

    String evalExpr(String exprInitial) {
        String exprString = exprInitial;

        if (op != null && op != ControlOp.OP_NOOP) {
            switch (op) {
            case OP_EQ:
                exprString += " == ";
                break;
            case OP_NE:
                exprString += " != ";
                break;
            case OP_GE:
                exprString += " >= ";
                break;
            case OP_LE:
                exprString += " <= ";
                break;
            case OP_LS:
                exprString += " << ";
                break;
            case OP_RS:
                exprString += " >> ";
                break;
            case OP_SUB:
                exprString += " - ";
                break;
            case OP_ADD:
                exprString += " + ";
                break;
            case OP_MUL:
                exprString += " * ";
                break;
            case OP_AND:
                exprString += " & ";
                break;
            case OP_NOOP:
                break;
            }
        }
        exprString += value != null ? value : "";
        return exprString;
    }

    String printExpression() {
        return evalExpr(path != null ? path.printPath() : "");
    }

    String cntlExpression() {
        if (path != null && path.isOneLevel() && path.equals("REMAINING"))
            return evalExpr("l_remaining_len");
        else
            return evalExpr(path != null ? "l_" + path.paramName() : "");
    }
}
