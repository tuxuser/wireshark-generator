/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

public class StructInfo {
    String name;
    int definingLine;
    Boolean referenced;
    Boolean missingChecked;
    Boolean referencesSetup;
    Boolean itemReferencesSetup;
    Boolean ettDeclared;
    Boolean ettDefined;
    Boolean hfDeclared;
    Boolean hfDefined;
    Boolean eiDefined;
    Boolean isTopLevel;
    ArrayList<Elt> elts;
    ProtoDetails protoDetails; // Only needed if top level

    // localVars are variables holding values that we need because
    // they are control vars. They might be truly local, ie in this
    // struct, or they might be from a struct we contain, or below a
    // struct we contain.
    ArrayList<ProtoPath> localVars;
    ArrayList<ProtoPath> localItemVars;

    // These are fields that are needed above us, so we must pass them
    // back when dissecting, and if we are only on the path to the
    // dissector that fetches them, pass them back.
    //
    // These are setup from setupNeededReferences when we have
    // determined them from below.
    ArrayList<ProtoPath> localNeededByAbove;

    // These are setup from setupNeededReferences when we have 
    // determined them from below and refer to items we need.
    ArrayList<ProtoPath> localItemsNeededByAbove;

    // These are the fields needed below us somewhere for their values.
    // We simply pass them on to the point where they are used. They
    // may be needed by us.
    //
    // We determine these from setupNeededReferences ...
    ArrayList<ProtoPath> neededByBelow;

    // These items are needed below us somewhere
    ArrayList<ProtoPath> itemsNeededByBelow;

    // We have to tell our parents what we need, possibly
    // All the way up the chain.
    StructInfo parent;  // Needed to allow us to tell it need
                        // access to field from it.

    public StructInfo(String name) {
        this.name = name;
        referenced = false;
        missingChecked = false;
        referencesSetup = false;
        itemReferencesSetup = false;
        ettDeclared = false;
        ettDefined = false;
        hfDeclared = false;
        hfDefined = false;
        eiDefined = false;
        isTopLevel = false;
        localVars = new ArrayList<ProtoPath>();
        localItemVars = new ArrayList<ProtoPath>();
        localNeededByAbove = new ArrayList<ProtoPath>();
        localItemsNeededByAbove = new ArrayList<ProtoPath>();
        neededByBelow = new ArrayList<ProtoPath>();
        itemsNeededByBelow = new ArrayList<ProtoPath>();
    }

    public Elt getElt(String eltName) {
        Debug.println(10, "//Gettng elt " + eltName + " in struct " +
                          name);
        for (Elt tmp:elts) {
            Elt matchingElt = tmp.getEltIf(eltName);
            Debug.println(10, "//Name of one elt: " + tmp.name);
            if (matchingElt != null)
                return tmp;
        }
        return null;
    }

    // Check that the path resolves to a base type or enum?
    // However, if the path refers to something above us we
    // use the stack to look up the starting point. We also
    // record the actual type for other uses.
    //  REWRITE to record the type correctly and the WS type.
    public boolean checkPathIsBaseType(ProtoPath path,
                                       ArrayList<StructInfo> stack) {
        Elt elt = null;

        Debug.println(10, "//checkPathIsBaseType-Struct name: " + name);
        if (path == null ||
            (path.componentName == null && path.pathType == null))
            return false;

        Debug.println(10, "//Checking Path is base type: " +
                          path.printPath());

        // Is it one of the special paths we allow, like REMAINING
        if (path.isOneLevel()) {
            if (path.equals("REMAINING"))
                return true;
		}

        ProtoPath tmp = path;
        String type = "";
        String wsType = "";
        StructInfo struct = this;

        while (tmp != null) { // We know it started non-null
            // We need to look up from the root or the current or the
            // parent of the current depending on where we are.
            switch (tmp.pathType) {
            case PATH_COMPONENT:
            case PATH_RELATIVE_CURRENT:
                Debug.println(10, "//Getting elt for " +
                                  tmp.componentName);
                elt = struct.getElt(tmp.componentName);
                break;
            case PATH_RELATIVE_PARENT:
                // This should happen only once at the init component
                for (StructInfo str:stack) {
                    Debug.println(10, "//Struct is " + str.name);
                }
                StructInfo localParent = stack.get(stack.size() - 2);
                Debug.println(10, "//Parent: " + localParent.name);
                Debug.println(10, "//Getting elt from parent for " +
                                  tmp.componentName);
                elt = localParent.getElt(tmp.componentName);
                break;
            }

            if (elt == null) {
                Debug.println(10, "//Elt not found ...");
                return false;
            }

            Debug.println(10, "//Elt is: " + elt.name);

            // If this is the last component, it must refer to a base type?
            // TOTO: Check for last comp but not base type
            if (tmp.next == null) {

                if (!elt.isBaseType()) {
                    Debug.println(10, "//Elt " + elt.name + " is not a " +
                                      "base type but is the last " +
                                      "component of " +
                                      path.printPath());
                    return false;
                }

                Debug.println(10, "//Setting type for: " +
                                  path.printPath() + " to: " +
                                  elt.getType());
                type = elt.getType();
                tmp.setType(type);
                // Also set the WireShark type
                int index = Util.typeIndex(type);

                if (index == -1) {
                    index = Util.getEnumTypeIndex(elt.getType());
                }

                if (index < 0) {
                    Debug.println(10, "//Unable to get index for " +
                                      "componentName: " +
                                      path.componentName +
                                      " type: " + path.type);
                    wsType = "Unknown";
                    path.setWsType(wsType);
                } else {
                    wsType = Util.getWsType(index);
                    path.setWsType(wsType);
                }
            } else {

                // If there is more in the path, it must refer to a struct
                if (!elt.isStruct()) {
                    Debug.println(10, "//Elt " + elt.name + "is not a " +
                                      "struct but is not the last " +
                                      "component of " +
                                      path.printPath());
                    return false;
                }

                struct = GenDissector.getGenerator().getStructs().get(elt.getType());
                Debug.println(10, "//Next struct: " + elt.getType());
                if (struct == null) {
                    Debug.println(10, "//Struct " + elt.getType() +
                              " not found but should exist!");
                    return false;
                }
            }

            tmp = tmp.next;
        }

        path.setType(type);
        path.setWsType(wsType); // Set this

        return true; // Must be good
    }

    // A struct with a single elt that is a base type is a base type.
    Boolean isBaseType() {
        if (elts != null && elts.size() == 1 && elts.get(0).isBaseType())
            return true;
        return false;
    }

    // Check for missing or bad struct elements (like recursion)
    // We also set referenced to true here because we know this
    // struct is referenced from something leading from the top
    // structured.
    Boolean checkMissing(ArrayList<StructInfo> stack) {
        Boolean missing = false;

        Debug.println(10, "//Checking for missing in struct: " +
                      name);
        referenced = true;

        // No need to check again if already checked? Unless it is
        // used from different path?
        if (missingChecked)
            return false;

        missingChecked = true;

        stack.add(this);

        for (Elt elt:elts) {
            Debug.println(10, "//Checking for missing elt in: " +
                              name + ": " + elt.name + ":" + elt.type);
            if (elt.checkMissing(stack, name)) {
                missing = true;
            }
        }

        stack.remove(stack.size() - 1); // Remove what we added above

        return missing;
    }

    // Is the string name a local var?
    Boolean isLocalVar(String name) {
        Debug.println(10, "  //Checking if " + name +
                          " is a local var");
        for (ProtoPath path:localVars) {
            Debug.println(10, "  //LocalVar: " + path.printPath());
            if (path.equals(name)) {
                Debug.println(10, "  //Name " + name + " matches!");
                return true;
            }
        }
        return false;
    }

    // Is the ProtoPath a local var?
    Boolean isLocalVar(ProtoPath path) {
        Debug.println(10, "  //Checking if " + path.printPath() +
                          " is a local var");
        for (ProtoPath path2:localVars) {
            Debug.println(10, "  //LocalVar: " + path2.printPath());
            if (path2.equals(path)) {
                Debug.println(10, "  //Path " + path.printPath() +
                                  " matches!");
                return true;
            }
        }
        return false;
    }

    // Is the string name a local var?
    Boolean isLocalItemVar(String name) {
        Debug.println(10, "  //Checking if " + name + " is a local item var");
        for (ProtoPath path:localItemVars) {
            Debug.println(10, "  //LocalItemVar: " + path.printPath());
            if (path.equals(name)) {
                Debug.println(10, "  //Name " + name + " is a local item var!");
                return true;
            }
        }
        return false;
    }

    // Is the ProtoPath a local item var?
    Boolean isLocalItemVar(ProtoPath path) {
        Debug.println(10, "  //Checking if " + path.printPath() +
                          " is a local item var");
        for (ProtoPath path2:localItemVars) {
            Debug.println(10, "  //LocalItemVar: " + path2.printPath());
            if (path2.equals(path)) {
                Debug.println(10, "  //Path " + path.printPath() +
                                  " matches!");
                return true;
            }
        }
        return false;
    }

    // The name is the last component name
    Boolean isNeededVar(String name) {
        Debug.println(10, "  //Checking if " + name +
                          " is a needed var");
        for (ProtoPath path:localNeededByAbove) {
          Debug.println(10, "  //NeededVar: " + path.printPath());
          if (path.equalsLast(name)) {
                Debug.println(10, "  //Name " + name + " matches!");
              return true;
          }
        }
        return false;
    }

    // The name is the last component name
    Boolean isNeededItem(String name) {
        Debug.println(10, "  //Checking if " + name +
                          " is a needed item");
        for (ProtoPath path:localItemsNeededByAbove) {
            Debug.println(10, "  //ItemNeededVar: " + path.printPath());
            if (path.equalsLast(name)) {
                Debug.println(10, "  //Item name " + name + " matches!");
                return true;
            }
        }
        return false;
    }

    void dumpNeeded() {
        for (ProtoPath path:localNeededByAbove) {
            System.out.println("path is needed " + path.printPath());
        }
    }

    // Get the path for the needed name
    ProtoPath getNeeded(String name) {
        Debug.println(10, "//Getting path for neededVar: " + name);
        for (ProtoPath path:localNeededByAbove) {
            if (path.equalsLast(name))
                return path;
        }
        return null;
    }

    // Get the path for the needed item name
    ProtoPath getNeededItem(String name) {
        Debug.println(10, "//Getting path for needed Item Var: " + name);
        for (ProtoPath path:localItemsNeededByAbove) {
            if (path.equalsLast(name))
                return path;
        }
        return null;
    }

    // Get the part for a local item var
    ProtoPath getLocalItemVar(String name) {
        Debug.println(10, "//Getting path for localItemVar: " + name);
        for (ProtoPath path:localItemVars) {
            if (path.equalsLast(name))
                return path;
        }
        return null;
    }

    // Get the path for a local var
    ProtoPath getLocalVar(String name) {
        Debug.println(10, "//Getting path for localVar: " + name);
        for (ProtoPath path:localVars) {
            if (path.equalsLast(name))
                return path;
        }
        return null;
    } 

    // set item needed in children recursive
    void setItemNeededInChildrenRec(ProtoPath subPath, ProtoPath path) {
        if (subPath.isOneLevel()) {
            Debug.println(10, "//Adding Item F path " + path.printPath() +
                          " to " + name);
            path.addIfNotPresent(localItemsNeededByAbove,
                                 "localItemsNeededByAbove");
            return;
        }

        Debug.println(10, "//Searching for " + subPath.componentName +
                      " in setItemNeededChildrenRec");
        Elt elt = getElt(subPath.componentName);
        Debug.println(10, "//Type of elt in setItemNeededInChildrenRec: " +
                      subPath.componentName + " is " + elt.getType());
        StructInfo struct = GenDissector.getGenerator().getStructs().get(elt.getType());
        struct.setItemNeededInChildrenRec(subPath.next, path);

		Debug.println(10, "//Adding Item I path " + path.printPath() +
                      " to " + name);
        path.addIfNotPresent(localItemsNeededByAbove, "localItemsNeededByAbove");
    }

    // setItemNeededInChildren so we know which items are needed
    void setItemNeededInChildren(ProtoPath path) {
        if (path.isOneLevel()) { // Nothing to do
            Debug.println(10, "//Path of item: " + path.printPath() +
                              " is one-level ...");
            return;
        }

        Debug.println(10, "//setItemNeededInChildren checking " + 
                          path.printPath());
        Elt elt = getElt(path.componentName);
        Debug.println(10, "//Type of elt in setItemNeededInChildren: " +
                          path.componentName + " is " + elt.getType());
        StructInfo struct = GenDissector.getGenerator().getStructs().get(elt.getType());

        if (struct != null)
            struct.setItemNeededInChildrenRec(path.next, path);
        else
            Debug.println(1, "//Could not find type of " +
                             path.componentName);
    }

    // Set needed in children recursive ...
    void setNeededInChildrenRec(ProtoPath subPath, ProtoPath path) {
        if (subPath.isOneLevel()) {
            Debug.println(10, "//Adding F path " + path.printPath() +
                          " to " + name);
            path.addIfNotPresent(localNeededByAbove,
                                 "localNeededByAbove");
            return;
        }

        Debug.println(10, "//Searching for " + subPath.componentName +
                      " in setNeededChildrenRec");
        Elt elt = getElt(subPath.componentName);
        Debug.println(10, "//Type of elt in setNeededInChildrenRec: " +
                      subPath.componentName + " is " + elt.getType());
        StructInfo struct = GenDissector.getGenerator().getStructs().get(elt.getType());
        struct.setNeededInChildrenRec(subPath.next, path);

		Debug.println(10, "//Adding I path " + path.printPath() +
                      " to " + name);
        path.addIfNotPresent(localNeededByAbove, "localNeededByAbove");
    }

    // Set needed in the children. Go down the path
    // to the second last element setting needed on the way
    void setNeededInChildren(ProtoPath path) {
        if (path.isOneLevel()) { // Should not be needed
            return;
        }

        Elt elt = getElt(path.componentName);
        Debug.println(10, "//Type of elt in setNeededInChildren: " +
                          path.componentName + " is " + elt.getType());
        StructInfo struct = GenDissector.getGenerator().getStructs().get(elt.getType());

        if (struct != null)
            struct.setNeededInChildrenRec(path.next, path);
        else
            Debug.println(1, "//Could not find type of " +
                             path.componentName);
    }

    void setWsType(ProtoPath path) {
        int index = Util.typeIndex(path.getType());

        if (index == -1) {
            index = Util.getEnumTypeIndex(path.getType());
        }

        if (index < 0) {
            Debug.println(10, "//Unable to get index for " +
                              "componentName: " + path.componentName +
                              " type: " + path.type);
            path.setWsType("Unknown");
        } else {
            Debug.println(10, "//Setting type for " +
                              path.printPath() + " to " +
                              Util.getWsType(index));
            path.setWsType(Util.getWsType(index));
        }
    }

    // Figure out the fields we need item references on. These are
    // used for Expert Infos and for adding things to.
    void setupNeededItemReferences(ArrayList<ProtoPath> needed) {
        // Figure out what is needed from our elements
        Debug.println(10, "//==== setupNeededItemreferences for: " + name);

        if (itemReferencesSetup) {
            Debug.println(10, "//==== ItemReferences already setup for" +
                              name);
            return;
        }

        itemReferencesSetup = true;

        for (Elt elt:elts) {
            Debug.println(10, "//Calling getItemNeeded for " +
                              elt.printDebugName());
            ArrayList<ProtoPath> needItem = elt.getItemNeeded();

            if (needItem != null) { // Add them to needed
                for (ProtoPath path:needItem) {
                    Debug.println(10, "//Adding itemNeeded for: " + name +
                                      " is: " + path.printPath());
                    setWsType(path);
                    path.addIfNotPresent(needed, "needed");
                }
            }
        }

        // Now set up the localItemsNeededAbove etc.
        ArrayList<ProtoPath> toRemove = new ArrayList<ProtoPath>();

        for (ProtoPath path:needed) {
            Debug.println(10, "//An item path in: " + name + " is " +
                              path.printPath());
            if (path.pathType != PathType.PATH_RELATIVE_PARENT) {
                Debug.println(10, "//Adding item " + path.printPath() +
                                  " is local to " + name +
                                  " or below");
                // Need a local item var here or below
                setWsType(path);
                path.addIfNotPresent(localItemVars, "localItemVars");

                // Add if not present and not at this level
                if (!path.isOneLevel()) {
                    Debug.println(10, "//Adding item " + path.printPath() +
                                      " to itemsNeededByBelow");
                    path.addIfNotPresent(itemsNeededByBelow,
                                         "itemsNeededByBelow");
                    setItemNeededInChildren(path);
                }
                toRemove.add(path);
            } else {
                // Remove the path relative bit and leave it.
                Debug.println(10, "Removing item parent relative for " +
                                  path.printPath());
                path.removeParentRelative();
                path.addIfNotPresent(itemsNeededByBelow, "itemsNeededByBelow");
            }
        }

        for (ProtoPath path:toRemove) {
            int index = needed.indexOf(path);
            needed.remove(index);
        }

        for (ProtoPath path:itemsNeededByBelow) {
            Debug.println(10, "//Item path needed below for: " + name +
                              " is " + path.printPath());
        }

        for (ProtoPath path:localItemsNeededByAbove) {
            Debug.println(10, "//Item path needed above for: " + name +
                              " is " +  path.printPath());
        }

        for (ProtoPath path:localItemVars) {
            Debug.println(10, "//Local Item var for: " + name + "  is " +
                              path.printPath());
        }
        Debug.println(10, "//==== end of setupNeededItemReferences for: " +
                          name);
    }

    // Figure out the fields that are needed by fields and structs
    // we refer to.
    void setupNeededReferences(ArrayList<ProtoPath> needed) {
        // First, find out what is needed from our elements
        Debug.println(10, "//==== setupNeededReferences for: " + name);

        if (referencesSetup) {
            Debug.println(10, "//==== References already setup for: " +
                              name);
            return;
        }

        referencesSetup = true;

        for (Elt elt:elts) {
            Debug.println(10, "//Calling getNeeded for " + elt.name);
            ArrayList<ProtoPath> need = elt.getNeeded();

            if (need != null) { // Add them to needed
                for (ProtoPath path:need) {
                    Debug.println(10, "//Adding needed for: " + name +
                                      " is: " + path.printPath());
                    // Only add if not aready present.
                    setWsType(path);
                    path.addIfNotPresent(needed, "needed");
                }
            }
        }

        // At this point we know the paths exist. We have to:
        // 1. Check if they are local, ie supplied  by us, in which
        //    case we need a local var
        // 2. Supplied below us, in which case we need to call the
        //    function in the structure that records what is needed.
        // 3. Supplied above us, in which case we simply leave the
        //    path in needed for it to be handled above us, but we
        //    remove one level of parent reference.
        ArrayList<ProtoPath> toRemove = new ArrayList<ProtoPath>();

        for (ProtoPath path:needed) {
            Debug.println(10, "//A path in: " + name + " is " +
                              path.printPath());
            if (path.pathType != PathType.PATH_RELATIVE_PARENT) {
                Debug.println(10, "//Path " + path.printPath() +
                                  " is local to " + name +
                                  " or below");
                // We need a local var for this field because
                // it is needed at our level or below us somewhere.
                Debug.println(10, "//Adding " + path.printPath() +
                             " to localVars");
                setWsType(path);
                path.addIfNotPresent(localVars, "localVars");

                // Only add it if it is not already there and it is
                // not at this level
                if (!path.isOneLevel()) {
                    Debug.println(10, "//Adding " + path.printPath() +
                                  " to neededByBelow");
                    path.addIfNotPresent(neededByBelow, "neededByBelow");
                    // Now, pass it on down the path
                    setNeededInChildren(path);
                }

                // Now, add the element we just decided we have
                // dealt with to the toRemove list.
                toRemove.add(path);
            } else {
                // Remove the path relative step but leave it in the
                // list so it will be handled above us. However,
                // We need it as a parameter as well so we need to add
                // it to neededByBelow.
                Debug.println(10, "Removing parent relative for " +
                                   path.printPath());
                path.removeParentRelative();
                path.addIfNotPresent(neededByBelow, "neededByBelow");
            }
        }

        for (ProtoPath path:toRemove) {
            int index = needed.indexOf(path);

            Debug.println(10, "//Removing " + path.printPath() +
                              " from needed");
            needed.remove(index);
        }

        // Now process those that are local. If they are single level
        // they are truly local, but if they are multi-level then we
        // need to set up the appropriate things.
        for (ProtoPath path:neededByBelow) {
            Debug.println(10, "//Path needed below for: " + name +
                              " is " + path.printPath());
        }

        for (ProtoPath path:localNeededByAbove) {
            Debug.println(10, "//Path needed above for: " + name +
                              " is " +  path.printPath());
        }

        for (ProtoPath path:localVars) {
            Debug.println(10, "//Local var for: " + name + "  is " +
                              path.printPath());
        }

        Debug.println(10, "//==== end of setupNeededReferences for: " +
                          name);
    }

    void print() {
        Debug.println(10, "/* struct " + name + " { */");
        for (Elt elt:elts) {
            elt.print();
        }
        Debug.println(10, "/* } */");
    }

    void generateInitialVars() {

    }

    // Generate HF decls for all elements in this struct. Do not
    // generate them for structures referred to from this struct.
    // We do not need one for the struct itself.
    void generateHfDecl(String cont) {
        if (!referenced) {
            Debug.println(10, "//Not generating hf decl for " +
                          name + " as it is not referenced!");
            return;
        }
        if (hfDeclared) {
            Debug.println(10, "//Not generating another hf decl for" +
                          name + " as it is already declared!");
            return;
        }
        Debug.println(10, "/* struct " + name + " */");
        hfDeclared = true;
        for (Elt elt:elts) {
            Debug.println(10, "//Generating hf for elt: " + elt.name);
            elt.generateHfDecl(name);
        }
        System.out.println("");
    }

    // Generate the hf fields for each elt in this array.
    // For structures, recursively call their generateHfArray.
    void generateHfArray(String searchName) {
        if (!referenced) {
            Debug.println(10, "//Not generating hf array for " +
                          name + " as it is not referenced!");
            return;
        }
        if (hfDefined) {
            Debug.println(10, "//Not generateing hf array again for " +
                          name + " as it has already been defined!");
            return;
        }
        Debug.println(10, "//Generating hf array for struct " + name);
        hfDefined = true;
        for (Elt elt:elts) {
            elt.generateHfArray(name, searchName);
        }
    }

    // Generate EI decls as appropriate for this struct.
    Boolean generateEiDecl() {
        if (!referenced) {
            Debug.println(10, "//Not generating ei decl for " +
                          name + " as it is not referenced!");
            return false;
        }

    	// Generate these ...
        for (Elt elt:elts) {
            if (elt.generateEiDecl()) {
                Debug.println(10, "//we should have an EI Array now for: " +
                                  name);
                eiDefined = true;
            }
        }
        System.out.println("");
        return eiDefined;
    }

    // Get the ett name for this struct
    String getEttName() {
        return "ett_" + name;
    }

    // Generate ETT decls as appropriate for this struct
    void generateEttDecl() {
        if (!referenced || ettDeclared) {
            Debug.println(10, "//Not generating ett decl for " +
                          name + " as it is already declared or  not referenced!");
            return;
        }
        Debug.println(10, "//Generating decl for " + getEttName());
        System.out.println("static gint " + getEttName() + " = -1;");
        ettDeclared = true;

        // There may be some elements that need them, like subdivided ones
        for (Elt elt:elts) {
            elt.generateEttDecl();
        }
        System.out.println("");
    }

    void generateEttArray() {
        if (!referenced || ettDefined) {
            Debug.println(10, "//Not generating ett array for " +
                          name + " as it is already defined or not referenced!");
            return;
        }
        System.out.println("    &" + getEttName() + ",");
        ettDefined = true;

        for (Elt elt:elts) {
            elt.generateEttArray();
        }
    }

    void generateEiArrayEntries(String searchPrefix) {
        for (Elt elt:elts) {
            elt.generateEiArray(searchPrefix);
        }
    }

    void generateEiArray(String searchPrefix, Boolean needEiArray) {
       Debug.println(10, "//eiDefined is " + eiDefined);
       if (needEiArray) {
           System.out.println("  static ei_register_info ei[] = {");
           generateEiArrayEntries(searchPrefix);
           System.out.println("  };");
           System.out.println("  expert_module_t *expert_" +
			      Util.printName(searchPrefix) + ";");
       } 
    }

    void generateEiArrayRegister(String protoName, Boolean needEiArray) {
        if (needEiArray) {
            String name = Util.printName(protoName);
            System.out.println("  expert_" + name + " = " +
                               "expert_register_protocol(proto_" +
                               name + ");");
            System.out.println("  expert_register_field_array(expert_" +
                               name + ", ei, array_length(ei));");
        }
    }

    // Generate dissector call for this struct. These three go together
    // in that they understand the parameters needed etc.
    void generateDissectCall(String indent, String tree, String label) {
        System.out.print(indent + "  offset = dissect_" + name +
                         "(tvb, pinfo, " + tree + ", offset");

        System.out.print(", \"" + Util.unquoteString(label) + "\"");
        // Now pass in the fields needed from above
        for (ProtoPath path:localNeededByAbove) {
            Debug.println(10, "//Adding " + path.paramName() +
                          " as variable");
            System.out.print(", &l_" + path.paramName());
        }
        // Now pass in the fields needed below us. They are all pointers
        for (ProtoPath path:neededByBelow) {
            if (!isLocalVar(path)) {
                Debug.println(10, "//Adding " + path.paramName() +
                                  " as a param");
                System.out.print(", l_" + path.paramName());
            }
        }

        // Now pass in items needed from below
	    for (ProtoPath path:itemsNeededByBelow) {
            if (!isLocalItemVar(path)) {
                Debug.println(10, "//Adding " + path.paramName() +
                                  " as an item param");
                System.out.print(", i_" + path.paramName());
            }
        }

        // The refs we need to return
        for (ProtoPath path:localItemsNeededByAbove) {
            System.out.print(", &i_" + path.paramName());
        }
        System.out.println(");");
    }

        // Generate dissector code for this struct
    void generateDissectDecl() {
        Boolean needsIter = false;

        Debug.println(10, "//Generating dissector for " + name);

        for (ProtoPath path:localVars) {
            Debug.println(10, "// " + path.printPath() +
                               "is a local var in " + name);
        }

        for (ProtoPath path:neededByBelow) {
            Debug.println(10, "//" + path.printPath() +
                          " needed by below in " + name);
        }

        for (ProtoPath path:localNeededByAbove) {
            Debug.println(10, "//" + path.printPath() +
                          " needed by above in " + name);
        }

        for (ProtoPath path:localItemVars) {
            Debug.println(10, "// " + path.printPath() +
                               "is a local item var in " + name);
        }

        for (ProtoPath path:itemsNeededByBelow) {
            Debug.println(10, "//" + path.printPath() +
                          " is an item needed by below in " + name);
        }

        for (ProtoPath path:localItemsNeededByAbove) {
            Debug.println(10, "// " + path.printPath() +
                              " is an item needed above in " + name);
        }

        if (!referenced) {
            Debug.println(10, "//Not generating dissect decl for " +
                          name + " as it is not referenced!");
            return;
        }

        System.out.print("static");
        System.out.println(" int");
        System.out.println("dissect_" + name + "(tvbuff_t *tvb, " +
                           "packet_info *pinfo _U_,");
        System.out.print("        proto_tree *tree");
        // If we are not top level we need more params
        if (isTopLevel)
            System.out.print(", void *data _U_");
        else
            System.out.print(", guint offset, char *dispName");
        // The ones we need from above but that are not local vars
        for (ProtoPath path:neededByBelow) {
            if (!isLocalVar(path)) {
                Debug.println(10, "//Adding param for " +
                                  path.printPath());
                System.out.print(", " + path.getWsType() + " l_" +
                                 path.paramName());
            }
        }
        // The ones we have to return
        for (ProtoPath path:localNeededByAbove) {
            System.out.print(", " + path.getWsType() + " *l_" +
                             path.paramName());
        }

        // Now pass in items needed from below
	    for (ProtoPath path:itemsNeededByBelow) {
            if (!isLocalItemVar(path)) {
                Debug.println(10, "//Adding " + path.paramName() +
                                  " as an item param");
                System.out.print(", proto_item *i_" + path.paramName());
            }
        }

        // The item refs we need to return
        for (ProtoPath path:localItemsNeededByAbove) {
            System.out.print(", proto_item **i_" + path.paramName());
        }
        System.out.println(")");
        System.out.println("{");

        // If we are top level we need a local offset.
        if (isTopLevel)
            System.out.println("  int offset = 0;");

        // Handle the need for iterators.
        for (Elt elt:elts) {
            if (elt.needsIterator()) {
                needsIter = true;
            }
            elt.generateInitialVars();
        }

	Debug.println(10, "//Generating local vars ... ");

        // localItemVars
        for (ProtoPath path:localItemVars) {
            Debug.println(10, "  // localItemVar: " + path.printPath());
            System.out.println("  proto_item *i_" + path.paramName() +
                               " = NULL;");
        }
        // localVars
        for (ProtoPath path:localVars) {
            Debug.println(10, "  // localVar: " + path.printPath());
            // Handle special variables
            if (path.isOneLevel() && path.equals("REMAINING")) {
                System.out.println("  guint l_remaining_len = 0;");
            } else
                System.out.println("  " + path.getWsType() + " l_" +
                                   path.paramName() + ";");
        }

        if (needsIter) {
            Debug.println(10, "//struct " + name + " needs iter");
            System.out.println("  int i = 0;");
            System.out.println("  proto_tree *list_tree = NULL;");
            System.out.println("  proto_item *pi = NULL;");
        }

        System.out.println("  proto_tree *sub_tree = NULL;");
        System.out.println("  proto_item *str_item = NULL;");
        System.out.println("  int saved_offset = offset;");
        System.out.println("");

        // Add column info
        if (isTopLevel) {
            System.out.println("  col_set_str(pinfo->cinfo, " +
                               "COL_PROTOCOL, " +
                               protoDetails.shortName + ");");
        }

        System.out.print("  sub_tree = proto_tree_add_subtree(" +
                         "tree, tvb, offset, -1, " + getEttName() +
                         ", &str_item, ");
        if (isTopLevel)
            System.out.println("\"" + name + "\");");
        else
            System.out.println("dispName);");

        for (Elt elt:elts) {
            elt.printDissector("sub_tree", name, this);
        }
        System.out.println("  proto_item_set_len(str_item, offset - " +
                           "saved_offset);");
        System.out.println("  return offset;");
        System.out.println("}");
        System.out.println("");
    }

    // Generate a forward declaration for this struct's dissector
    void generateForwDecl() {
        if (!referenced) {
            Debug.println(10, "//Not generating forward decl for " +
                          name + " as it is not referenced!");
            return;
        }
        System.out.println("static int");
        System.out.println("dissect_" + name + "(tvbuff_t *tvb, " +
                           "packet_info *pinfo _U_,");
        System.out.print("        proto_tree *tree");
        if (isTopLevel)
            System.out.print(", void *data");
        else
            System.out.print(", guint offset, char *dispName");
        // The ones we need from above by this struct or below.
        for (ProtoPath path:neededByBelow) {
            if (!isLocalVar(path)) {
                System.out.print(", " + path.getWsType() + " " +
                             path.paramName());
            }
        }
        // The ones we have to return
        for (ProtoPath path:localNeededByAbove) {
            System.out.print(", " + path.getWsType() + " *" +
                             path.paramName());
        }

        // Now pass in items needed from below
	    for (ProtoPath path:itemsNeededByBelow) {
            if (!isLocalItemVar(path)) {
                Debug.println(10, "//Adding " + path.paramName() +
                                  " as an item param");
                System.out.print(",proto_item *i_" + path.paramName());
            }
        }

        // The refs we need to return
        for (ProtoPath path:localItemsNeededByAbove) {
            System.out.print(", proto_item **i_" + path.paramName());
        }
        System.out.println(");");
        System.out.println("");
    }

    // Generate the fields needed by Lua.
    void generateLuaStructFields(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
				 ArrayList<String> expertNames) {
        String prefix = searchPrefix + "." + name;
        for (Elt elt:elts) {
            if (elt.isStruct()) {
               structsLeft.add(elt.getType());
            } else {
                // We only want fields qualified by struct they are in
                // and any switch arms
                elt.generateLuaFieldDef(name, prefix, structsLeft, fieldNames, expertNames);
            }
        }
    }

    void generateLuaDissectCall(String indent, String tree, String label) {
        System.out.print(indent + "    offset");

        // Those needed by above
        for (ProtoPath path:localNeededByAbove) {
            System.out.print(", l_" + path.paramName());
        }

        // Items needed by above
        for (ProtoPath path:localItemsNeededByAbove) {
            System.out.print(", i_" + path.paramName());
        }

        System.out.print(" = dissect_" + name +
                         "(buffer, pinfo, " + tree + ", offset, " + label);

        // Ones needed below
        for (ProtoPath path:neededByBelow) {
            if (!isLocalVar(path)) {
                System.out.print(", l_" + path.paramName());
            }
        }

        // Ones needed below
        for (ProtoPath path:itemsNeededByBelow) {
            if (!isLocalItemVar(path)) {
                System.out.print(", i_" + path.paramName());
            }
        }

        System.out.println(")");
    }

    // Generate the Lua code required for a struct.
    // Should pass in the struct ... so we can handle needed variables.
    void generateLuaStructDissect(String protoName,
                                  ArrayList<String> structsLeft) {
        Debug.println(10, "-- Generating code for struct " + name);
        if (!referenced) {
            Debug.println(10, "-- Not generating dissect decl for " +
                          name + " as it is not referenced!");
            return;
        }

        // If we are a top-level struct, need an offset.
        if (isTopLevel) {
            System.out.println("function " + protoName + "_proto" +
                               ".dissector(buffer," +
                               "pinfo, tree)\n");

            System.out.println("    local offset = 0\n");

            System.out.println("    pinfo.cols['protocol'] = " +
                               Util.quoteString(protoName));
        } else {
            System.out.print("function dissect_" + name +
                             "(buffer, pinfo, tree, offset, label");

            for (ProtoPath path:neededByBelow) {
                if (!isLocalVar(path)) {
                    System.out.print(", l_" + path.paramName());
                }
            }

            for (ProtoPath path:itemsNeededByBelow) {
                if (!isLocalItemVar(path)) {
                    System.out.print(", i_" + path.paramName());
                }
            }

            System.out.println(")\n");
            System.out.println("    local saved_offset = offset");
            for (ProtoPath path:localNeededByAbove) {
                System.out.println("    local l_" + path.paramName());
            }
        }

        for (ProtoPath path:localVars) {
            Debug.println(10, "    -- localVar: " + path.printPath());
            // Handle special variables
            if (path.isOneLevel() && path.equals("REMAINING")) {
                System.out.println("    local l_remaining_len");
            } else {
                //System.out.println("  " + path.getWsType() + " l_" +
                //                   path.paramName() + ";");
            }
        }

        // Create a tree for the struct but based on whether top level or not
        String tree = "t_" + name;
        if (isTopLevel) {
            System.out.println("    local " + tree + " = tree:add(" +
                               protoName + "_proto, buffer())");
        } else {
            System.out.println("    local " + tree +
                               " = tree:add(buffer(offset,1), " +
                               "label)");
        }

        for (Elt elt:elts) {
            if (elt.isStruct()) {
                StructInfo struct = GenDissector.getGenerator().getStructs().get(elt.getType());

                Debug.println(10, "-- Generating Lua Dissect with tree " +
                                 name);
                struct.generateLuaDissectCall("    ", tree,
                                               Util.quoteString(elt.name));
                structsLeft.add(elt.getType());
            } else {
                elt.generateLuaStructDissect(tree, name, name, structsLeft);
            }
        }

        if (!isTopLevel) {
            System.out.println("    t_" + name +
                               ":set_len(offset - saved_offset)\n");

            System.out.print("    return offset");

            for (ProtoPath path:localNeededByAbove) {
                System.out.print(", l_" + path.paramName());
            }

            for (ProtoPath path:localItemsNeededByAbove) {
                System.out.print(", i_" + path.paramName());
            }

            System.out.println("");
        }

        System.out.println("end\n");
    }
}
