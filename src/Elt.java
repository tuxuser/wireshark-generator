/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018, 2019 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

/*
 * A structure elt's type must be a standard type or another struct.
 *
 * An element will have a type but can have sub-elts if a base type.
 * The types can be:
 * 1. A base type
 * 2. A struct defined here or externally?
 * 3. A base type but split into sub-elts?
 * 4. An array.
 * 5. Other defined structures.
 */
 abstract class Elt {
    String name;
    EltType type;
    int definingLine;

    // Format our name for printing.
    String printName(String inputName) {
        if (inputName == null) {
            return "Type: " + getType() + " has no name";
        } else {
            return inputName.toLowerCase().replace(' ', '_').replace("\"", "");
        }
    }

    String printName() {
        return printName(name);
    }

    // Remove quotes from the name and replace space with underscore
    String codeName() {
        return name.replaceAll("\"","").replaceAll(" ", "_");
    }

    // Quote a name. Strip any quotes and add quotes.
    String quoteName() {
        return "\"" + name.replaceAll("\"","") + "\"";
    }

    abstract void print();
    abstract String printDebugName();
    abstract Elt getEltIf(String eltName);
    abstract Boolean checkMissing(ArrayList<StructInfo> stack, String cont);
    abstract Boolean isBaseType();
    abstract Boolean isStruct();
    abstract Boolean isFunction();
    abstract String getType();
    abstract Boolean needsIterator();
    abstract void generateInitialVars();
    abstract void generateHfDecl(String cont);
    abstract void generateHfArray(String cont, String searchName);
    abstract Boolean generateEiDecl();
    abstract void generateEttDecl();
    abstract void generateEttArray();
    abstract void generateEiArray(String searchPrefix);
    abstract void printDissector(String tree, String containerName, StructInfo container); // Print dissector code
    abstract ArrayList<ProtoPath> getNeeded();
    abstract ArrayList<ProtoPath> getItemNeeded();

    abstract String getWsDisplayInfo();
    abstract void generateLuaFieldDef(String fieldPrefix, String searchPrefix, ArrayList<String> structsLeft, ArrayList<String> fieldNames, ArrayList<String> expertNames);
    abstract void generateLuaStructDissect(String tree, String protoName, String fieldPrefix, ArrayList<String> structsLeft);
}
