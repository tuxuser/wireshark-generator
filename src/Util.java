/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

class Util {

    // These three or so need to be modified together. void is only in
    // the first, not most of the others.
    static final String[] baseTypes = {
        "bit", "boolean", "uint4", "byte", "char", "string",
        "uint8", "int8", "uint16",
        "int16", "uint32", "int32", "uint64", "int64", "ether", "ether_t",
        "oui_t", "uint16_le", "uint16_be", "uint32_le", "uint32_be",
        "uint64_le", "uint64_be", "int16_le", "int16_be",
        "int32_le", "int32_be", "int64_le", "int64_be",
        "void",
    };

    static final String[] fieldTypes = {
        "FT_BOOLEAN", "FT_BOOLEAN", "FT_UINT8", "FT_BYTES", "FT_CHAR",
        "FT_STRING", "FT_UINT8", "FT_INT8", "FT_UINT16",
        "FT_INT16", "FT_UINT32",
        "FT_INT32", "FT_UINT64", "FT_INT64", "FT_ETHER", "FT_ETHER",
        "FT_UINT24", "FT_UINT16", "FT_UINT16", "FT_UINT32", "FT_UINT32",
        "FT_UINT64", "FT_UINT64", "FT_INT16", "FT_INT16",
        "FT_INT32", "FT_INT32", "FT_INT64", "FT_INT64",
    };

    static final String[] luaTypes = {
        "boolean", "boolean", "uint8", "bytes", "char", "string",
        "uint8", "int8", "uint16",
        "int16", "uint32", "int32", "uint64", "int64", "ether", "ether",
        "uint32", "uint16", "uint16", "uint32", "uint32",
        "uint64", "uint64", "int16", "int16",
        "int32", "int32", "int64", "int64",
    };

    static final String[] fieldBase = {
        "size", "size", "BASE_DEC", "BASE_NONE", "BASE_NONE", "BASE_NONE",
        "BASE_DEC",
        "BASE_DEC", "BASE_DEC", "BASE_DEC", "BASE_DEC", "BASE_DEC",
        "BASE_DEC", "BASE_DEC", "BASE_NONE", "BASE_NONE", "BASE_OUI",
        "BASE_DEC", "BASE_DEC", "BASE_DEC", "BASE_DEC",
        "BASE_DEC", "BASE_DEC", "BASE_DEC", "BASE_DEC",
        "BASE_DEC", "BASE_DEC", "BASE_DEC", "BASE_DEC",
    };

    static final String[] luaFieldBase = {
        "size", "size", "base.DEC", "base.NONE", "base.NONE", "base.NONE",
        "base.DEC",
        "base.DEC", "base.DEC", "base.DEC", "base.DEC", "base.DEC",
        "base.DEC", "base.DEC", "base.NONE", "base.NONE", "base.OUI",
        "base.DEC", "base.DEC", "base.DEC", "base.DEC",
        "base.DEC", "base.DEC", "base.DEC", "base.DEC",
        "base.DEC", "base.DEC", "base.DEC", "base.DEC",
    };

    static int typeIndex(String type) {
        for (int i = 0; i < baseTypes.length; i++) {
            if (baseTypes[i].equals(type))
                return i;
        }
        return -1;  // Should we throw an exception here?
    }

    // This currently takes a base type, but will have to change to
    // accomodate the need to add things to the type, like BASE_RANGE_STRING
    static String getWsFieldType(String type) {
        int index = typeIndex(type);

        if (index == -1) {
            // Is it an Enum?
            EnumInfo enumInfo = GenDissector.getGenerator().getEnums().get(type);

            if (enumInfo != null) {
                Debug.println(10, "//It's an enum, returning type for " +
                              enumInfo.getType());
                return Util.getWsFieldType(enumInfo.getType());
            }

            Debug.println(10, "//Returning FT_UNKNOWN for " + type);
            return "FT_UNKNOWN";
        } else
            return fieldTypes[index];
    }

    static final String[] bitLengths = {
        "1", "1", "4", "8", "8", "8", "8", "8", "16", "16", "32", "32",
        "64", "64", "48", "48", "24", "16", "16", "32", "32", "64", "64",
        "16", "16", "32", "32", "64", "64", "0",
    };

    // Get the bit lenghth of a type, return -1 if unknown
    static int getTypeBitLen(String type) {
        int index = typeIndex(type);

        if (index == -1)
            return -1;
        else
            return Integer.parseInt(bitLengths[index]);
    }

    // Get the display field. Some need special handling to add things
    // like BASE_CUSTOM etc. However, if the bit length is less than 8
    // it gets rounded up to 8? This is so we can handle bits in
    // places like enums.
    static String getWsDisplayType(String type) {
        int index = typeIndex(type);

        if (index == -1)
            return "BASE_UNKNOWN";
        else {
            if (fieldBase[index].equals("size")) {
                if (Integer.parseInt(bitLengths[index]) < 8)
                    return "8";
                else
                    return bitLengths[index];
            } else
                return fieldBase[index];
        }
    }

    //Get the Lua display field
    static String getLuaDisplayType(String type) {
        int index = typeIndex(type);

        if (index == -1)
            return "base.UNKNOWN";
        else {
            if (luaFieldBase[index].equals("size")) {
                if (Integer.parseInt(bitLengths[index]) < 8)
                    return "8";
                else
                    return bitLengths[index];
            } else
                return luaFieldBase[index];
        }
    }

    static final int[] baseTypeLens = {
         1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 4, 4, 8, 8, 6, 6, 3, 2, 2, 4, 4,
         8, 8, 2, 2, 4, 4, 8, 8, 0,
    };

    // Note, void is listed as a base type but is never fetched or displayed.
    static final String[] wsTypes = {
        "gboolean", "gboolean", "guint8", "guint8", "gchar", "gchar", "guint8",
        "gint8",
        "guint16", "gint16", "guint32", "gint32", "guint64", "gint64",
        "guint64", "guint64", "guint32", "guint16", "guint16",
        "guint32", "guint32", "guint64", "guint64",
        "gint16", "gint16", "gint32", "gint32", "gint64", "gint64", ""
    };

    static String getWsType(int index) {
        return wsTypes[index];
    }

    static final String[] encodings = {
        "ENC_NA", "ENC_NA", "ENC_NA", "ENC_NA", "ENC_NA", "ENC_NA", "ENC_NA",
        "ENC_NA",
        "COMP", "COMP", "COMP", "COMP", "COMP", "COMP", "ENC_NA", "ENC_NA",
        "ENC_NA",
        "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN", "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN",
        "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN", "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN",
        "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN", "ENC_LITTLE_ENDIAN", "ENC_BIG_ENDIAN",
    };

    static final String[] be_getters = {
        "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8",
        "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8",
        "tvb_get_ntohs", "tvb_get_ntohs", "tvb_get_ntohl", "tvb_get_ntohl",
        "tvb_get_ntoh64", "tvb_get_ntoh64",
        "tvb_get_ntoh48", "tvb_get_ntoh48", "tvb_get_ntoh24",
        "tvb_get_letohs", "tvb_get_ntohs", "tvb_get_letohl", "tvb_get_ntohl",
        "tvb_get_letoh64", "tvb_get_ntoh64",
    };

    static final String[] le_getters = {
        "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8",
        "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8", "tvb_get_guint8",
        "tvb_get_letohs", "tvb_get_letohs", "tvb_get_letohl", "tvb_get_letohl",
        "tvb_get_letoh64", "tvb_get_letoh64",
        "tvb_get_letoh48", "tvb_get_letoh48", "tvb_get_letoh24",
        "tvb_get_letohs", "tvb_get_ntohs", "tvb_get_letohl", "tvb_get_ntohl",
        "tvb_get_letoh64", "tvb_get_ntoh64",
    };

    static final String[] beLuaAdders = {
         "add", "add", "add", "add",
         "add", "add", "add", "add",
         "add", "add", "add", "add",
         "add", "add",
         "add", "add", "add",
         "add_le", "add", "add_le", "add",
         "add_le", "add",
    };

    static final String[] leLuaAdders = {
         "add", "add", "add", "add",
         "add", "add", "add", "add",
         "add_le", "add_le", "add_le", "add_le",
         "add_le", "add_le",
         "add_le", "add_le", "add_le",
         "add_le", "add", "add_le", "add",
         "add_le", "add",
    };

    static int baseTypeLen(String type) {
        for (int i = 0; i < baseTypes.length; i++) {
            if (baseTypes[i].equals(type))
                return baseTypeLens[i];
        }
        return 0;  // Should we throw an exception here?
    }

    static String baseTypeLenString(String type) {
        int len = baseTypeLen(type);

        if (len < 0)
            return "Unknown";
        else
            return Integer.toString(len);
    }

    static String getEncoding(String type, EndianType endian) {
        for (int i = 0; i < baseTypes.length; i++) {
            if (baseTypes[i].equals(type)) {
                if (encodings[i].equals("COMP")) {
                    if (endian == EndianType.ENDIAN_LITTLE)
                        return "ENC_LITTLE_ENDIAN";
                    else
                        return "ENC_BIG_ENDIAN";
                } else
                    return encodings[i];
            }
        }
        return "ENC_NA";  // Should we throw an exception?
    }

    static String baseTypeGetter(String type, EndianType endian) {
        for (int i = 0; i < baseTypes.length; i++) {
            if (baseTypes[i].equals(type)) {
                if (endian == EndianType.ENDIAN_LITTLE)
                    return le_getters[i];
                else
                    return be_getters[i];
            }
        }
        return "tvb_get_unknown"; // Should we throw an exception?
    }

    static String getLuaAdder(String type, EndianType endian) {
        for (int i = 0; i <baseTypes.length; i++) {
            if (baseTypes[i].equals(type)) {
                if (endian == EndianType.ENDIAN_LITTLE)
                    return leLuaAdders[i];
                else
                    return beLuaAdders[i];
            }
        }
        return "add_UNKNOWN";
    }

    static Boolean isSimpleArrayType(String type) {
        if (type.equals("char") || type.equals("byte"))
            return true;
        return false;
    }

    // It is not yet clear how I want to handle arrays of some types. While and
    // array of bytes should not be inserted via a sub-tree, possibly arrays of
    // other objects should be.
    static final String[] nonArrayTypes = {
        "byte", "char", "string"
    };

    // Check that a name is a base type from above
    static Boolean isBaseType(String type) {
        for (String candidate:baseTypes) {
            if (candidate.equals(type)) {
                return true;
            }
        }
        return false;
    }

    static Boolean isNonArrayType(String type) {
        for (String candidate:nonArrayTypes) {
            if (candidate.equals(type)) {
                return true;
            }
        }
        return false;
    }

    // The print name a component has. Strips quotes and replaces space with
    // underscore.
    static String printName(String compName) {
        return compName.toLowerCase().replace(' ', '_').replace("\"", "");
    }

    // Quote a name. Strip any quotes and add quotes.
    static String quoteString(String str) {
        return "\"" + str.replaceAll("\"","") + "\"";
    }

    // Unquote a string.
    static String unquoteString(String name) {
        return name.replace("\"", "");
    }

    //
    // Figure out whether or not we need to have a conversion field and
    // return it. Returns null if none needed.
    static String getWsFieldConvert(String type) {
        EnumInfo enumInfo = GenDissector.getGenerator().getEnums().get(type);

        // If it is bit or binary return TFS(&...) else RVALS...
        if (enumInfo != null) {
            return enumInfo.getWsFieldConvert();
        }

        return null;
    }

    static Boolean isEnum(String type) {
        return GenDissector.getGenerator().getEnums().get(type) != null;
    }

    static int getEnumTypeIndex(String type) {
        int index = -1;
        EnumInfo tmpEnum = GenDissector.getGenerator().getEnums().get(type);

        if (tmpEnum != null)
            index  = typeIndex(tmpEnum.type);

        return index;
    }

    static String luaType(String type) {
        int index = typeIndex(type);

        return luaTypes[index];
    }
}
