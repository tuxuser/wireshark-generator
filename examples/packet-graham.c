GenDissector initializing
// C Dissector generated from ../graham.proto
#include "config.h"
#include <epan/packet.h>

static int proto_grahams_proto = -1;
static int hf_request_reply_data_id = -1;
static int hf_request_reply_data_some_string = -1;
static int hf_request_reply_data_request_reply_data_0 = -1;
static int hf_request_reply_data_request_reply_data_1 = -1;
static int hf_request_reply_data_data_string = -1;
static int hf_request_reply_data_unknown_data_request = -1;

static int hf_grahams_proto_pdu_func = -1;
static int hf_grahams_proto_pdu_len = -1;
static int hf_grahams_proto_pdu_grahams_proto_pdu_0 = -1;
static int hf_grahams_proto_pdu_grahams_proto_pdu_1 = -1;
static int hf_grahams_proto_pdu_grahams_proto_pdu_2 = -1;
static int hf_grahams_proto_pdu_grahams_proto_pdu_3 = -1;
static int hf_grahams_proto_pdu_grahams_proto_pdu_4 = -1;
static int hf_grahams_proto_pdu_unknown_function_data = -1;

static gint ett_request_reply_data = -1;

static gint ett_grahams_proto_pdu = -1;

enum funcs_enum {
  CONNECT        = 20,
  CONNECT_ACK    = 21,
  REQUEST_DATA   = 40,
  REQUEST_REPLY  = 41,
  DISCONNECT     = 60,
  DISCONNECT_ACK = 61,
};

static const range_string funcs_enum_rvals[] = {
  { 1, 19, "Reserved" },
  { 20, 20, "connect" },
  { 21, 21, "connect_ack" },
  { 22, 39, "Reserved" },
  { 40, 40, "request_data" },
  { 41, 41, "request_reply" },
  { 42, 59, "Reserved" },
  { 60, 60, "disconnect" },
  { 61, 61, "disconnect_ack" },
  { 62, 255, "Reserved" }
};

enum data_enum {
  READ_SHORT  = 0,
  READ_LONG   = 1,
  READ_STRING = 2,
};

static const range_string data_enum_rvals[] = {
  { 0, 0, "read short" },
  { 1, 1, "read long" },
  { 2, 2, "read string" },
  { 3, 255, "Reserved" }
};

static int
dissect_request_reply_data(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_grahams_proto_pdu(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, void *data);


static int
dissect_request_reply_data(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 id;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_request_reply_data, &str_item, dispName);
  id = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_request_reply_data_id, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_request_reply_data_some_string, tvb, offset, 14, ENC_NA);
  offset += 14;
  switch (id) {
  case READ_SHORT:
    proto_tree_add_item(sub_tree, hf_request_reply_data_request_reply_data_0, tvb, offset, 3, ENC_LITTLE_ENDIAN);
    offset += 3;
    break;

  case READ_LONG:
    proto_tree_add_item(sub_tree, hf_request_reply_data_request_reply_data_1, tvb, offset, 2, ENC_LITTLE_ENDIAN);
    offset += 2;
    break;

  case READ_STRING:
    proto_tree_add_item(sub_tree, hf_request_reply_data_data_string, tvb, offset, 15, ENC_NA);
    offset += 15;
    break;

  default:
    proto_tree_add_item(sub_tree, hf_request_reply_data_unknown_data_request, tvb, offset, len - 3, ENC_NA);
    offset += len - 3;
    break;

  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_grahams_proto_pdu(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, void *data _U_)
{
  int offset = 0;
  guint8 func;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  col_set_str(pinfo->cinfo, COL_PROTOCOL, "grahams_proto");
  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_grahams_proto_pdu, &str_item, "grahams_proto_pdu");
  func = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_func, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_len, tvb, offset, 2, ENC_BIG_ENDIAN);
  offset += 2;
  switch (func) {
  case CONNECT:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_grahams_proto_pdu_0, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    break;

  case CONNECT_ACK:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_grahams_proto_pdu_1, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    break;

  case DISCONNECT:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_grahams_proto_pdu_2, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    break;

  case DISCONNECT_ACK:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_grahams_proto_pdu_3, tvb, offset, 4, ENC_BIG_ENDIAN);
    offset += 4;
    break;

  case REQUEST_DATA:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_grahams_proto_pdu_4, tvb, offset, 1, ENC_NA);
    offset += 1;
    break;

  case REQUEST_REPLY:
    offset = dissect_request_reply_data(tvb, pinfo, sub_tree, offset, "Data");
    break;

  default:
    proto_tree_add_item(sub_tree, hf_grahams_proto_pdu_unknown_function_data, tvb, offset, len - 3, ENC_NA);
    offset += len - 3;
    break;

  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

void
proto_register_grahams_proto(void)
{
  static hf_register_info hf[] = {
    {&hf_grahams_proto_pdu_func,
     {"func", "grahams_proto.grahams_proto_pdu.func",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(funcs_enum_rvals), 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_len,
     {"len", "grahams_proto.grahams_proto_pdu.len",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_grahams_proto_pdu_0,
     {"id", "grahams_proto.grahams_proto_pdu.id",
      FT_UINT32, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_grahams_proto_pdu_1,
     {"id", "grahams_proto.grahams_proto_pdu.id",
      FT_UINT32, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_grahams_proto_pdu_2,
     {"id", "grahams_proto.grahams_proto_pdu.id",
      FT_UINT32, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_grahams_proto_pdu_3,
     {"id", "grahams_proto.grahams_proto_pdu.id",
      FT_UINT32, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_grahams_proto_pdu_4,
     {"Request", "grahams_proto.grahams_proto_pdu.request",
      FT_UNKNOWN, BASE_UNKNOWN|BASE_RANGE_STRING, RVALS(data_enum_rvals), 0, NULL, HFILL }},

    {&hf_request_reply_data_id,
     {"id", "grahams_proto.grahams_proto_pdu.id",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(data_enum_rvals), 0, NULL, HFILL }},

    {&hf_request_reply_data_some_string,
     {"some_string", "grahams_proto.grahams_proto_pdu.some_string",
      FT_STRING, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_request_reply_data_request_reply_data_0,
     {"Data Short", "grahams_proto.grahams_proto_pdu.data_short",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_request_reply_data_request_reply_data_1,
     {"Data Long", "grahams_proto.grahams_proto_pdu.data_long",
      FT_UINT32, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_request_reply_data_data_string,
     {"Data String", "grahams_proto.grahams_proto_pdu.data_string",
      FT_STRING, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_request_reply_data_unknown_data_request,
     {"Unknown data request", "grahams_proto.grahams_proto_pdu.unknown_data_request",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_grahams_proto_pdu_unknown_function_data,
     {"Unknown function data", "grahams_proto.grahams_proto_pdu.unknown_function_data",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

  };

  static gint *ett[] = {
    &ett_grahams_proto_pdu,
    &ett_request_reply_data,
  };

  proto_grahams_proto = proto_register_protocol("Graham's Protocol", "grahams_proto", "grahamp");

  proto_register_field_array(proto_grahams_proto, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));
}

void
proto_reg_handoff_grahams_proto(void)
{
  static dissector_handle_t grahams_proto_handle;
  grahams_proto_handle = create_dissector_handle(dissect_grahams_proto_pdu, proto_grahams_proto);
  dissector_add_uint("ethertype", 0x893C, grahams_proto_handle);
}
