SETUP

You will need Antlr4. You can get Antlr4 from:

   git clone https://github.com/antlr/antlr4.git

There are instructions on how to install Antlr here:

   https://github.com/antlr/antlr4/blob/master/doc/getting-started.md

In addition, you can install a packaged version of Antlr4 on some Linux
distributions.

BUILDING THE DISSECTOR GENERATOR

Since the generated code, and thus any JAR file, is specific to the version
of the Antlr runtime, you will have to build the generator after installing
Antlr4 (see below for instructions on Windows):

Use the following commands:

    clone the repository
    cd wireshark-generator/src
    antlr4 WiresharkGenerator.g4
    javac *.java

See the notes below for building on Windows or Linux.

BUILDING A DISSECTOR

If you just want to build a dissector using the existing code you can
use the jar file included in the top level directory.

You will need Antlr installed, so follow the dirrections in the above URL.

Use the following command to build a dissector (from the src directory):

    java GenDissector <unput-file>

eg:

    java GenDissector ../examples/graham3.proto

You can build a Lua dissector by adding -l before the proto description file:

    java GenDissector -l ../examples/graham3.proto

You can redirect the output to a file of your choice.

There is more info in the doc directory on writing proto files. See
doc/basics.txt for more info.

BUILDING ON WINDOWS

You will have to install Antlr4 on Windows as well as set up a couple of
batch files.

This involves:

    1. Placing the latest version of Antlr4 complete jar file on your system.
    2. Setting the CLASSPATH environment variable correctly
    3. Creating two batch files
    4. Setting the PATH environment variable correctly
    5. Starting a DOS command prompt

The Antlr download page is here: https://www.antlr.org/download.html

See the following page for how to do most of this, including setting the 
CLASSPATH environment variable:

    https://levlaz.org/setting-up-antlr4-on-windows/

Appending the location of your batch files to the PATH environment variable
is similar.

BUILDING ON UNIX

You will have to do this once, generally.

You will need the JDK so you have javac. I installed JDK 9.9, however,
I also had to mess around with the alternatives system it seems.

Add these to your ~/.bashrc

    alias antlr4='java -jar /usr/local/lib/antlr-4.7-complete.jar'
    alias grun='java org.antlr.v4.runtime.misc/TestRig'

However, if you have installed Antlr4 as a package, you will not need the
first one above. Also, you will need something like:

    alias grun='java -cp /usr/share/java/antlr4/antlr4.jar:/usr/share/java/antlr4/antlr4-runtime.jar org.antlr.v4.gui.TestRig'

Note, the above alias may not be needed on Debian-derived distributions.

BUILDING
    cd wireshark-generator/src
    antlr4 WiresharkGenerator.g4
    javac *.java

RUNNING

    java GenDissector <input-file>

eg:

    java GenDissector ../graham3.proto

You can also generate LUA dissectors from the same description:

    java GenDissector -l ../graham3.proto

ADDING A LUA DISSECTOR TO WIRESHARK

If you are building a Lua dissector, this is simple. Just place the Lua
code for your dissector in the location specified in:

    Help->About Wireshark->Folders

under Personal Lua Plugins

and then click Analyze->Reload Lua Plugins.

You will get errors if there are any syntax errors in the generated code.

You will also get runtime error messages when dissecting code, however, I 
think I have eliminated all of those.

ADDING A C DISSECTOR TO WIRESHARK

You will need a Wireshark development environment.

1. Place your new dissector in epan/dissectors
2. Edit epan/dissectors/CMakeLists.txt
3. Add a line to the DISSECTOR_SRC set (look for "^set(DISSECTOR_SRC")
   that specifies your new dissector. Ie,

         ${CMAKE_CURRENT_SOURCE_DIR}/<your-c-dissector-src>.c

4. If you have not already done so, create a build directory outside the
   source
5. cd to your build directory and run 'cmake <path-to-wireshark-source>`
6. make

These instructions are for Unix. For Windows it is more involved.

Also, depending on the Linux Distro you have you may need to install and use
cmake3 rather than cmake. This is true of RHEL6 and 7 and CentOS 6 and 7.

The file pkt-gen/generated.pcapng has packets that graham3.proto can
handle that you can use for testing.

Send me email if you have questions or problems.
